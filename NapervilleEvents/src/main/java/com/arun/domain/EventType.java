package com.arun.domain;

public enum EventType {
	CHILDREN("event.chidlren"),
	WALKS_RUNS("event.walks_runs"),
	ANNUAL("event.annual"),
	FUNDRAISER("event.fundraiser"),
	HOLIDAY("event.holiday"),
	THEATER_MUSIC("event.theater_music"),
	ANDERSON_BOOKSTORE("event.anderson_bookstore"),
	GENERAL("event.general"),
	NAPER_SETTLEMENT("event.naper_settlement"),
	FAMILY("event.family"),
	INVALID("event.invalid");
	
	private final String eventType;
	
	EventType(String eventType){
		this.eventType= eventType;
	}
	
	public String getEventTypeValue(){
		return eventType;
	}
	
	public static EventType getEvent(String s){
		EventType returnValue = EventType.INVALID;
		
		for(EventType item: EventType.values()){
			if(s.equals(item.name())) returnValue = item;
		}
		
		return returnValue;
	}
	
}
