package com.arun.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import com.arun.common.NapervilleEvent;

/*
 * DAO class to handle persistence
 */

@Repository
@Transactional
public class EventDAO {

	@PersistenceContext
	private EntityManager entityManager;	

	public void create(NapervilleEvent event) {
	entityManager.persist(event);
	return;
	}

	@SuppressWarnings("unchecked")
	public List<NapervilleEvent> getEventsByType(String eventType) {
		return  entityManager.createQuery(
				"from NapervilleEvent where eventType = :eventType")
				.setParameter("eventType", eventType)
				.getResultList();
	}	  
	  
	
}
