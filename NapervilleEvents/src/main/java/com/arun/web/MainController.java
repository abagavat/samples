package com.arun.web;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.arun.domain.EventType;
import com.arun.service.EventLoader;

/*
 * Main Controller
 */

@Controller
public class MainController {

	@Autowired
	public EventLoader eventloader;
	
	@PostConstruct
	public void init(){
		eventloader.loadEvents();
	}
	
    @ModelAttribute("eventTypes")
    public List<EventType> eventTypes() {
    	
    	List<EventType> list =  new ArrayList<>(Arrays.asList(EventType.values()));
    	list.remove(EventType.INVALID);
    
        return list;
    }
	
    @RequestMapping("/")
    public String entry(Model model) {
        return "main";
    }	
	
    @RequestMapping(value = "/getevents")
    public String getEvents(Model model, @RequestParam("eventType") String eventType) {
    	model.addAttribute("events", eventloader.getEventsList(eventType));
    	return "results :: resultsList";
    }

}
