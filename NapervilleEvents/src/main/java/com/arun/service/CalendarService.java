package com.arun.service;

import java.io.IOException;
import java.io.InputStream;

import org.springframework.context.ResourceLoaderAware;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Service;

import net.fortuna.ical4j.data.CalendarBuilder;
import net.fortuna.ical4j.data.ParserException;
import net.fortuna.ical4j.model.Calendar;

@Service
public class CalendarService implements ResourceLoaderAware{
	
	private ResourceLoader resourceLoader;

	public void setResourceLoader(ResourceLoader resourceLoader) {
		this.resourceLoader = resourceLoader;
	}	

	/*
	 * Retrieve calendar as a resource	
	 */
	public Calendar getCalendar(String location){
		Calendar calendar = null;
	
		try {
			Resource resource =  resourceLoader.getResource(location);
			InputStream  inputStream = resource.getInputStream();			
			CalendarBuilder builder = new CalendarBuilder();
			calendar = builder.build(inputStream);			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParserException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		
		return calendar;
	}
	
}
