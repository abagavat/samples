package com.arun.service;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.*;
import org.springframework.core.env.Environment;

import com.arun.common.NapervilleEvent;
import com.arun.dao.EventDAO;
import com.arun.domain.EventType;

import net.fortuna.ical4j.filter.Filter;
import net.fortuna.ical4j.filter.PeriodRule;
import net.fortuna.ical4j.model.*;

@org.springframework.stereotype.Component
public class EventLoader {
	
	@Autowired
	public CalendarService calendarService;
	
	@Autowired
	public EventDAO eventDAO;	

	@Autowired
	public Environment env;	
	
	public Map<String,ArrayList<NapervilleEvent>> eventsListMap = new HashMap<>();
	
	/*
	 * Retrieve events from database for event type	
	 */
	public List<NapervilleEvent> getEventsList(String eventType){
		//return eventsListMap.get(eventType);
		return eventDAO.getEventsByType(eventType);
	}

	/*
	 * Load events from calendar to database	
	 */	
	@SuppressWarnings({ "rawtypes", "unchecked", "deprecation" })
	public void loadEvents(){
		
		Set<String> uniqueIDs = new HashSet<>();

    	Base64.Encoder encoder = null;
		MessageDigest md5 = null;		
		
	    try {
	    	encoder = Base64.getEncoder();
			md5 = MessageDigest.getInstance("MD5");
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
		
		for(EventType eventType : EventType.values()){
			if(eventType == EventType.INVALID) continue;

			//Get Calendar for event type
			Calendar calendar =  calendarService.getCalendar(env.getProperty(eventType.getEventTypeValue()));
			ArrayList<NapervilleEvent> eventsList = new ArrayList<>();
			
			java.util.Calendar today = java.util.Calendar.getInstance();
			today.set(java.util.Calendar.HOUR_OF_DAY, 0);
			today.clear(java.util.Calendar.MINUTE);
			today.clear(java.util.Calendar.SECOND);			
			
			// Filter events happening in the next 90 days
			Period period = new Period(new DateTime(today.getTime()), new Dur(90, 0, 0, 0));
			Filter filter = new Filter(new PeriodRule<>(period));				
			Collection<Component> c =  filter.filter(calendar.getComponents(Component.VEVENT));

			for (Iterator<Component> i = c.iterator(); i.hasNext();) {
			    Component component = (Component) i.next();
			    
			    String uid = null;
			    String summary = component.getProperty("SUMMARY").getValue();
			    String description = StringUtils.defaultIfEmpty(component.getProperty("DESCRIPTION").getValue(),"");
			    
			    //Filter duplicates
			    uid = encoder.encodeToString(md5.digest((summary + description).getBytes()));			    
			    if(uniqueIDs.contains(uid)) continue;
			    else uniqueIDs.add(uid);
			    
		        NapervilleEvent event = new NapervilleEvent();
		        if (component.getProperty("SUMMARY") != null){
		        	event.setEventType(eventType.name());
			        event.setSummary(summary);
			        event.setDescription(description);
			        event.setLocation(StringUtils.defaultIfEmpty(component.getProperty("LOCATION").getValue(),""));
			        eventDAO.create(event);
			        eventsList.add(event);
		        }			        
			}			
			
			//eventsListMap.put(eventType.name(), eventsList);
		}		
	}
}
